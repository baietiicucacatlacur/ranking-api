var mongoose = require('mongoose');

var GameSchema   = new mongoose.Schema({
    name: String,
    type: String,
    userId: String
});

module.exports = mongoose.model('Game', GameSchema);