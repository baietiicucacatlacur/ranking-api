var Game = require("../models/game");

exports.postGame = function (req, res) {
    var game = new Game();
    game.name = req.body.name;
    game.type = req.body.type;
    game.userId = req.user._id;

    // Save the beer and check for errors
    game.save(function (err) {
        if (err) {
            res.send(err);
        }
        res.json({message: 'Game added!', data: game});
    });
};

exports.getGames = function (req, res) {
    Game.find({userId: req.user._id}, function (err, beers) {
        if (err) {
            res.send(err);
        }
        res.json(beers);
    });
};

exports.getGame = function (req, res) {
    Game.find({userId: req.user._id, _id: req.params.game_id}, function (err, game) {
        if (err) {
            res.send(err);
        }
        res.json(game);
    });
};

exports.putGame = function (req, res) {
    Game.update({ userId: req.user._id, _id: req.params.game_id }, {name: req.body.name, type: req.body.type}, function (err, num) {
        if (err)
            res.send(err);
        res.json({ message: num.nModified + ' game(s) updated' });
    });
};

exports.deleteGame = function (req, res) {
    Game.remove({ userId: req.user._id, _id: req.params.game_id }, function (err) {
        if (err)
            res.send(err);

        res.json({message: 'Game removed!'});
    });
};