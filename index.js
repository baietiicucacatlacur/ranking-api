var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var ejs = require('ejs');
var passport = require('passport');
var session = require('express-session');
var oauth2Controller = require('./controllers/oauth2');

var gameController = require('./controllers/game');
var userController = require('./controllers/user');
var authController = require('./controllers/auth');
var clientController = require('./controllers/client');

mongoose.connect('mongodb://localhost:27017/ranking');
var app = express();
app.use(session({
    secret: '12345',
    saveUninitialized: true,
    resave: false
}));

var port = process.env.PORT || 3000;
var router = express.Router();

app.use('/api', router);

router.get('/', function (req, res) {
    res.json({message: 'Success!'});
});

router.route('/users')
    .post(userController.postUsers)
    .get(authController.isAuthenticated, userController.getUsers);

router.route('/games')
    .post(authController.isAuthenticated, gameController.postGame)
    .get(authController.isAuthenticated, gameController.getGames);

router.route('/games/:game_id')
    .get(authController.isAuthenticated, gameController.getGame)
    .put(authController.isAuthenticated, gameController.putGame)
    .delete(authController.isAuthenticated, gameController.deleteGame);

router.route('/clients')
    .post(authController.isAuthenticated, clientController.postClients)
    .get(authController.isAuthenticated, clientController.getClients);

// Create endpoint handlers for oauth2 authorize
router.route('/oauth2/authorize')
    .get(authController.isAuthenticated, oauth2Controller.authorization)
    .post(authController.isAuthenticated, oauth2Controller.decision);

// Create endpoint handlers for oauth2 token
router.route('/oauth2/token')
    .post(authController.isClientAuthenticated, oauth2Controller.token);

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(passport.initialize());

app.listen(port, function () {
    console.log('Listening on port ' + port);
});
